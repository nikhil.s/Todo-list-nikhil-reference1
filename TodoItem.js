import React, {
    Component
} from "react";

const TodoItem = (props) => {
    const changeStatus= () => {
        props.changeStatus(props.todo.id)
    }
    if(props.todo.status){
    return  <div style={{textDecoration:"line-through"}} onClick={changeStatus}>{props.todo.taskname} 
       </div>
       
    }
    else{
        return <div onClick={changeStatus}> {props.todo.taskname}</div>
    }
}
export default TodoItem;