var config = {
  mode : 'development',
  entry: './main.jsx',
  output: {
     filename: 'dist/index.js',
  },
  devServer: {
     inline: true,
     port: 8080
  },
  module: {
     rules: [
       {
           test: /\.jsx$/,
           exclude: /node_modules/,
           use: {
             loader: "babel-loader"
           }
       },
       {
           test: /\.js$/,
           exclude: /node_modules/,
           use: {
             loader: "babel-loader"
           }
       },
       {
        test: /\.css$/,
        use: [
          {loader: "style-loader"},
          {loader: "css-loader"},
     ],
      }
     ]
  }
}
module.exports = config;
