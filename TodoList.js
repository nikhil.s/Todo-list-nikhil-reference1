import React, {
    Component
} from "react";
import TodoItem from './TodoItem';

const TodoList = (props) => {
return (
    <div>{props.todos.map(todo => <TodoItem todo={todo} changeStatus={props.changeStatus}/>)}
    </div>)
}

export default TodoList;