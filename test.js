class A {

    constructor() {
        this.a = 10;
        // this.cde = this.cde.bind(this)
    }

    abc = () => {
        console.log(this.a)
        console.log(this)
    }

    cde = () => {
        this.abc()
    }

}

let a = new A();
// a.cde();

let d = a.cde;
console.log(d);
console.log(a.cde);

//a.cde();
d();