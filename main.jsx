import React from 'react';
import ReactDom from 'react-dom';
import TodoApp from "./TodoApp";


var destination = document.getElementById('app');

ReactDom.render(
        <TodoApp />,
      destination
)
